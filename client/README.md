# TODO
- Column widths.
- Check if id? voor interface make that we don't need to do as and maybe everyList;
- Check if posting entire GrocerieList in Grocerie is the right way.
- Move navigation to common app module; 
- Check clean code unused code, outlining, working tests and so on;

IDEA

Later:
- Write tests;
- Button size/center vertical;
- Fix errors in tests;
- Filter groceries server side for byGrocerieList;

```
docker run -it --rm --publish 4200:4200 --mount type=bind,source=$PWD,target=/app --mount type=volume,source=angular_node_modules,target=/app/node_modules setup-booschapp bash
ng serve --host 0.0.0.0 --disable-host-check
```
