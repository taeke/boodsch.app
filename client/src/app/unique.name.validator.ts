import { ValidatorFn, AbstractControl } from '@angular/forms';

export interface ObjectWithName {
    name: string;
    id: number;
}

export function uniqueNameValidator(collection: ObjectWithName[]): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
        const forbidden = collection.some(objectWithName => objectWithName.name === control.value);
        return forbidden ? { forbiddenName: { value: control.value } } : null;
    };
}

