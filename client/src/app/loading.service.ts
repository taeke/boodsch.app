import { Injectable } from '@angular/core';
import { Observable, forkJoin, of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { LoadingSpinnerComponent } from './common-app/loading-spinner/loading-spinner.component';
import { AlertDialogComponent } from './common-app/alert-dialog/alert-dialog.component';
import { catchError } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {
    constructor(public dialog: MatDialog) { }

    load(obj: any, calls: Observable<any>[], resultMapping: string[], callOnFinish: () => void): void {
        const loadingSpinnerRef = this.dialog.open(LoadingSpinnerComponent, {
            panelClass: 'transparent',
            disableClose: true
        });

        forkJoin(calls)
            .pipe(catchError((response: HttpResponse<string>) => { 
                this.dialog.open(AlertDialogComponent, { data: { message: response.body }});
                return of(resultMapping.map(field => obj[field])); 
            }))
            .subscribe(resultList => {
                loadingSpinnerRef.close();
                resultMapping.forEach((value, index) => {
                    obj[value] = resultList[index];
                });

                callOnFinish();
            });
    }
}
