import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GroceriesComponent } from './groceries/groceries.component';
import { ProductsComponent } from './products/products.component';
import { StoresComponent } from './stores/stores.component';
import { DepartmentsComponent } from './departments/departments.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/groceries',
        pathMatch: 'full'
    },
    {
        path: 'groceries',
        component: GroceriesComponent,
        data: {
            title: 'BoodschApp boodschappen'
        }
    },
    {
        path: 'products',
        component: ProductsComponent,
        data: {
            title: 'BoodschApp produkten'
        }
    },
    {
        path: 'stores',
        component: StoresComponent,
        data: {
            title: 'BoodschApp winkels'
        }
    },
    {
        path: 'departments',
        component: DepartmentsComponent,
        data: {
            title: 'BoodschApp afdelingen'
        }
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
