import { Injectable } from '@angular/core';
import { ObjectWithName } from '../unique.name.validator';

@Injectable({
    providedIn: 'root'
})
export class CommonToAppService {
    public compare(a: ObjectWithName, b: ObjectWithName): number {
        if (a.id > b.id) {
            return -1;
        }

        if (a.id < b.id) {
            return 1;
        }

        return 0;
    }
}
