import { TestBed } from '@angular/core/testing';

import { CommonToAppService } from './common-to-app.service';

describe('CommonToAppService', () => {
  let service: CommonToAppService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommonToAppService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
