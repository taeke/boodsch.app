import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingSpinnerComponent } from './loading-spinner/loading-spinner.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';

@NgModule({
  declarations: [LoadingSpinnerComponent, AlertDialogComponent],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatDialogModule
  ]
})
export class CommonAppModule { }
