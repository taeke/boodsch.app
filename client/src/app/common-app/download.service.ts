import { Injectable } from '@angular/core';
import { Grocerie } from '../groceries/groceries.service';

interface ExtendedGrocerie extends Grocerie {
	extended: string
}

export enum DownLoadtype {
	perStore,
	perDepartment
}

@Injectable({
	providedIn: 'root'
})
export class DownloadService {
	donwload(groceries: Grocerie[], name: string, downloadType: DownLoadtype) {
		const element = document.createElement('a');
		let extendedGroceries = this.toExtendedGroceries(groceries, downloadType);
		const component = encodeURIComponent(this.createDownload(extendedGroceries, name, downloadType));
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + component);
        element.setAttribute('download', name);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);   		
	}

	private toExtendedGroceries(groceries: Grocerie[], downloadType: DownLoadtype): ExtendedGrocerie[] {
		switch(downloadType) {
			case DownLoadtype.perStore: 
				return groceries.map(grocerie => { 
					return { ...grocerie,  extended: `${grocerie.amount} ${grocerie.name}` }; 
				});		
			case DownLoadtype.perDepartment: 
				return groceries.map(grocerie => { 
					return { ...grocerie,  extended: `${grocerie.amount} ${grocerie.name}    ${grocerie.storeName}` }; 
				});		
		}
	}

	private createDownload(groceries: ExtendedGrocerie[], name: string, downloadType: DownLoadtype): string {
		let result: string[] = [];
		result.push(name);
		let transFormItems;
		switch(downloadType) {
			case DownLoadtype.perStore: 
				transFormItems = this.transFormItemsPerStore;
				break;
			case DownLoadtype.perDepartment:
				transFormItems = this.transFormItemsPerDepartment;			
		}

		const recursiveTransForm = (groceries: ExtendedGrocerie[], index: number): void => {
			this.unique(groceries, transFormItems[index].fieldName).forEach(name => {
				result.push(transFormItems[index].transForm(name));
				if (index < transFormItems.length - 1) {
					let filtered = groceries.filter(grocerie => this.filter(grocerie, name, index, transFormItems));
					recursiveTransForm(filtered, index + 1);
				}
			});
		}

		recursiveTransForm(groceries, 0);
		return result.join('\n');
	}

	private unique(groceries: ExtendedGrocerie[], fieldName: string): string[] {
		let names =  groceries.map(item => item[fieldName])
		return names.filter((value, index, names) => names.indexOf(value) === index);
	}

	private transFormItemsPerStore = [
		{ fieldName: 'storeName', transForm: (item: string) => `\n${item}\n=========================` },
		{ fieldName: 'departmentName', transForm: (item: string) => `Afdeling : ${item}` },
		{ fieldName: 'extended', transForm: (item: string) => `   ${item}` }];

	private transFormItemsPerDepartment = [
		{ fieldName: 'departmentName', transForm: (item: string) => `\nAfdeling : ${item}\n=========================` },
		{ fieldName: 'extended', transForm: (item: string) => `   ${item}` }];

	private filter(grocerie: ExtendedGrocerie, name: string, index: number, transFormItems): boolean {
		return grocerie[transFormItems[index].fieldName] === name;
	}
}
