import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Store } from '../stores.service';

@Component({
    selector: 'app-stores-table',
    templateUrl: './stores-table.component.html',
    styleUrls: ['./stores-table.component.css']
})
export class StoresTableComponent {
    @Input() stores: Store[];
    @Output() deleteStoreEvent = new EventEmitter<Store>();

    displayedColumns: Array<string> = ['name', 'delete'];

    delete(store: Store): void {
        this.deleteStoreEvent.emit(store);
    }
}
