import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { StoresTableComponent } from './stores-table.component';

describe('StoresTableComponent', () => {
	let component: StoresTableComponent;
	let fixture: ComponentFixture<StoresTableComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [StoresTableComponent],
			imports: [
				NoopAnimationsModule,
				MatPaginatorModule,
				MatSortModule,
				MatTableModule,
			],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(StoresTableComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should compile', () => {
		expect(component).toBeTruthy();
	});
});
