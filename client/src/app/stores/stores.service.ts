import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ObjectWithName } from '../unique.name.validator';
import { map } from 'rxjs/operators';
import { CommonToAppService } from '../common-app/common-to-app.service';

export interface Store extends ObjectWithName {}

@Injectable({
    providedIn: 'root'
})
export class StoresService {
    private storesUrl = 'api/stores';

    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(private http: HttpClient, private commonToAppService: CommonToAppService) { }

    getStores(): Observable<Store[]> {
        return this.http
            .get<Store[]>(this.storesUrl)
            .pipe(map(stores => stores.sort(this.commonToAppService.compare)));
    }

    addStore(store: Store, stores: Store[]): Observable<Store[]> {
        return this.http
            .post<Store>(this.storesUrl, store, this.httpOptions)
            .pipe(map(store =>{
                stores.push(store);
                return stores.slice(0).sort(this.commonToAppService.compare);
            }));
    }

    deleteStore(store: Store, stores: Store[]): Observable<Store[]> {
        return this.http
            .delete<Store>(this.storesUrl + "/" + store.id, this.httpOptions)
            .pipe(map(() => {
                stores.splice(stores.indexOf(store), 1);
                return stores.slice(0).sort(this.commonToAppService.compare);
            }));
    }
}
