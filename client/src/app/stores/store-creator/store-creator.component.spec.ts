import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { StoreCreatorComponent } from './store-creator.component';

describe('StoreCreatorComponent', () => {
    let component: StoreCreatorComponent;
    let fixture: ComponentFixture<StoreCreatorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [StoreCreatorComponent],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StoreCreatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
