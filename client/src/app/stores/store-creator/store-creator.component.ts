import { Component, Input, ViewChild, OnChanges, SimpleChanges } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { uniqueNameValidator } from '../../unique.name.validator';
import { ErrorStateMatcherService } from '../../error-state-matcher.service';
import { Store} from '../stores.service'

@Component({
    selector: 'app-store-creator',
    templateUrl: './store-creator.component.html',
    styleUrls: ['./store-creator.component.css']
})
export class StoreCreatorComponent implements OnChanges {
    @Output() newStoreEvent = new EventEmitter<string>();
    @Input() stores: Store[];
    @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

    newStoreGroup = new FormGroup({
        name: new FormControl('', [Validators.required])
    });

    get nameControl() { return this.newStoreGroup.get('name'); }

    constructor(public matcher: ErrorStateMatcherService) { }

    ngOnChanges(changes: SimpleChanges): void {
        if ('stores' in changes && typeof this.stores !== 'undefined' && this.stores.length > 0) {
            this.nameControl.setValidators([Validators.required, uniqueNameValidator(this.stores)]);
        }
    }

    onSubmit(): void {
        let name = this.newStoreGroup.value.name;
        if (name && name != '' && !this.stores.find(item => item.name === name)) {
            this.newStoreEvent.emit(name);
            setTimeout(() => this.formGroupDirective.resetForm(), 200);
        }
    }
}
