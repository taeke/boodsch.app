import { Component, OnInit } from '@angular/core';
import { StoresService, Store } from './stores.service';
import { LoadingService } from '../loading.service';
import { ProductsService, Product } from '../products/products.service';
import { AlertDialogComponent } from '../common-app/alert-dialog/alert-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-stores',
    templateUrl: './stores.component.html',
    styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {
    stores: Store[] = [];
    products: Product[] = [];

    constructor(
        private loadingService: LoadingService, 
        private storesService: StoresService,
        private productsService: ProductsService,
        public dialog: MatDialog) { }

    ngOnInit(): void {
        let calls = [this.storesService.getStores(), this.productsService.getProducts()];
        this.loadingService.load(this, calls, ['stores', 'products'], () => {});
    }

    add(name: string): void {
        name = name.trim();
        if (name) { 
            let call = [this.storesService.addStore({ name } as Store, this.stores)];
            this.loadingService.load(this, call, ['stores'], () => {});
        }
    }

    delete(store: Store): void {
        if(this.canDelete(store, this.products.filter(product => product.everyList && product.lastStoreName === store.name))) {
            let call = [this.storesService.deleteStore(store, this.stores)];
            this.loadingService.load(this, call, ['stores'], () => {});
        }
    }

    private canDelete(store: Store, productsWithStore: Product[]): boolean {
        if(productsWithStore.length > 0) {
            let message = `De winkel ${ store.name } kan niet verwijderd worden want de volgende producten worden elke keer
                toegevoegd en gebruiken deze winkel: ${ productsWithStore.map(product => product.name).join(",") }. 
                Verwijder het vinkje elke keer toevoegen en voeg ze eenmalig handmatig toe aan een boodschappenlijst met een 
                andere winkel om dit op te lossen.`;
            this.dialog.open(AlertDialogComponent, { data: { message: message }});
            return false;  
        } 

        return true;
    }
}
