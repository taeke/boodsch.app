import { TestBed } from '@angular/core/testing';
import { StoresService } from './stores.service';
import { HttpClient } from '@angular/common/http';

class MockHttpClient {}

describe('StoresService', () => {
    let service: StoresService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers:[{ provide: HttpClient, useClass: MockHttpClient }]
        });
        service = TestBed.inject(StoresService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
