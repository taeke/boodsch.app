import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StoresComponent } from './stores.component';
import { StoresService } from './stores.service';
import { Observable, of } from 'rxjs';

class MockStoresService {
    getStores() { return of({}); }
}

describe('StoresComponent', () => {
    let component: StoresComponent;
    let fixture: ComponentFixture<StoresComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [StoresComponent],
            providers:[
                { provide: StoresService, useClass: MockStoresService }
            ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StoresComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
