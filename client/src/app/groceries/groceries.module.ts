import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { GroceriesComponent } from './groceries.component';
import { GroceriesListSelectorComponent } from './groceries-list-selector/groceries-list-selector.component';
import { GrocerieCreatorComponent } from './grocerie-creator/grocerie-creator.component';
import { GroceriesTableComponent } from './groceries-table/groceries-table.component';
import { GrocerieListPipe } from './grocerie-list.pipe';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDialogModule } from '@angular/material/dialog';

import { ObjectWithName } from '../unique.name.validator';
import { Product } from '../products/products.service';
import { Grocerie } from '../groceries/groceries.service';
import { RowEditComponent } from './groceries-table/row-edit/row-edit.component';

declare global {
    interface Array<T> {
        hasNoEmptyStrings(): boolean;
        difference(other: Array<ObjectWithName>): Array<ObjectWithName>;
        hasName(name: string): boolean;
        notHasName(name: string): boolean;
        getNameOrEmpty(name: string): string;
    }
}

Array.prototype.difference = function (other: Array<ObjectWithName>) {
    let _self = this as Array<Product>;
    return _self.filter(objectWithName => {
        return !other.some(otherObjectWithName => objectWithName.name === otherObjectWithName.name);
    });
};

Array.prototype.hasName = function (name: string) {
    let _self = this as Array<Grocerie>;
    return _self.some(item => item.name === name);
};

Array.prototype.notHasName = function (name: string) {
    let _self = this as Array<Grocerie>;
    return !_self.hasName(name);
};

Array.prototype.hasNoEmptyStrings = function () {
    let _self = this as Array<string>;
    return _self.every(str => Boolean(str));
};

@NgModule({
    declarations: [GroceriesComponent, GroceriesListSelectorComponent, GrocerieCreatorComponent, GroceriesTableComponent, GrocerieListPipe, RowEditComponent],
    providers: [DatePipe],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatFormFieldModule,
        MatSelectModule,
        MatButtonModule,
        MatAutocompleteModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        ReactiveFormsModule
    ]
})
export class GroceriesModule { }
