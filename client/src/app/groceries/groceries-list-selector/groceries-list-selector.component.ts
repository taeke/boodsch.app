import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GrocerieList } from '../grocerie-lists.service';
import { DownLoadtype } from '../../common-app/download.service';

interface DownloadChoice {
    downloadType: DownLoadtype;
    text: string;
}

@Component({
    selector: 'app-groceries-list-selector',
    templateUrl: './groceries-list-selector.component.html',
    styleUrls: ['./groceries-list-selector.component.css']
})
export class GroceriesListSelectorComponent {
    @Input() grocerieLists: GrocerieList[] = [];
    @Input() grocerieList: GrocerieList;
    @Output() grocerieListChange = new EventEmitter<GrocerieList>();
    @Output() newGrocerieListEvent = new EventEmitter();
    @Output() downloadGrocerieListEvent = new EventEmitter();
    downloadChoice: DownloadChoice;

    downloadChoices: DownloadChoice[] = [{
        downloadType: DownLoadtype.perDepartment,
        text: 'Per afdeling'
    },{
        downloadType: DownLoadtype.perStore,
        text: 'Per winkel'
    }]

    constructor() {
        this.downloadChoice = this.downloadChoices[0];
    }

    get selected(): GrocerieList { return this.grocerieList; }
    set selected(grocerieList: GrocerieList) { this.grocerieListChange.emit(grocerieList); }

    new(): void {
        this.newGrocerieListEvent.emit();
    }

    download() : void {
        this.downloadGrocerieListEvent.emit(this.downloadChoice.downloadType);
    }
}
