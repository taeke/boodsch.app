import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GroceriesListSelectorComponent } from './groceries-list-selector.component';
import { GrocerieListsService } from '../grocerie-lists.service';
import { Observable, of } from 'rxjs';

class MockGrocerieListsService {
    getGrocerieLists() {
        return of([]);
    }
}

describe('GroceriesListSelectorComponent', () => {
    let component: GroceriesListSelectorComponent;
    let fixture: ComponentFixture<GroceriesListSelectorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GroceriesListSelectorComponent],
            providers:[
                { provide: GrocerieListsService, useClass: MockGrocerieListsService }
            ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GroceriesListSelectorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
