import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { GrocerieList } from './grocerie-lists.service';
import { ObjectWithName } from '../unique.name.validator';
import { CommonToAppService } from '../common-app/common-to-app.service';

export interface Grocerie extends ObjectWithName {
    grocerieList: GrocerieList;
    amount: number;
    storeName: string;
    departmentName: string;
}

export class GrocerieImpl {
    static create(grocerieList: GrocerieList, amount: number, productName: string, storeName: string, departmentName: string): Grocerie {
        return { 
            grocerieList: grocerieList, 
            amount: amount,
            name: productName, 
            storeName: storeName, 
            departmentName: departmentName } as Grocerie
    }
}

@Injectable({
    providedIn: 'root'
})
export class GroceriesService {
    private grocerieUrl = 'api/groceries';

    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(private http: HttpClient, private commonToAppService: CommonToAppService) { }

    getGroceries(): Observable<Grocerie[]> {
        return this.http
            .get<Grocerie[]>(this.grocerieUrl)
            .pipe(map(groceries => groceries.sort(this.commonToAppService.compare)));
    }

    getGroceriesByGrocerieList(grocerieList: GrocerieList): Observable<Grocerie[]> {
        return this.http
            .get<Grocerie[]>(this.grocerieUrl)
            .pipe(map(groceries => { 
                return groceries
                    .filter(grocerie => grocerie.grocerieList.id === grocerieList.id)
                    .sort(this.commonToAppService.compare);
            }));
    }

    addGrocerie(grocerie: Grocerie, groceries: Grocerie[]): Observable<Grocerie[]> {
        return this.http
            .post<Grocerie>(this.grocerieUrl, grocerie, this.httpOptions)
            .pipe(map(grocerie => {
                groceries.push(grocerie);
                return groceries.slice(0).sort(this.commonToAppService.compare);
            }));
    }

    updateGrocerie(updatedGrocerie: Grocerie, groceries: Grocerie[]): Observable<Grocerie[]> {
        return this.http
            .put<Grocerie>(this.grocerieUrl + "/" + updatedGrocerie.id, updatedGrocerie, this.httpOptions)
            .pipe(map(grocerie => {
                groceries[groceries.indexOf(groceries.find(g => g.name === grocerie.name))] = grocerie;
                return groceries.slice(0).sort(this.commonToAppService.compare);
            }));
    }

    deleteGrocerie(grocerie: Grocerie, groceries: Grocerie[]): Observable<Grocerie[]> {
        return this.http
            .delete<Grocerie>(this.grocerieUrl + "/" + grocerie.id, this.httpOptions)
            .pipe(map(() => {
                groceries.splice(groceries.indexOf(grocerie), 1);
                return groceries.slice(0).sort(this.commonToAppService.compare);
            }));
    } 
}
