import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GroceriesComponent } from './groceries.component';
import { GroceriesService } from './groceries.service';

class MockGroceriesService {}

describe('GroceriesComponent', () => {
    let component: GroceriesComponent;
    let fixture: ComponentFixture<GroceriesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GroceriesComponent],
            providers:[
                { provide: GroceriesService, useClass: MockGroceriesService }
            ],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GroceriesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
