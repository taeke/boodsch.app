import { GrocerieListPipe } from './grocerie-list.pipe';
import { DatePipe } from '@angular/common';

describe('GrocerieListPipe', () => {
    it('create an instance', () => {
        const pipe = new GrocerieListPipe(new DatePipe('utc'));
        expect(pipe).toBeTruthy();
    });
});
