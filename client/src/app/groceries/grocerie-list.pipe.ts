import { Pipe, PipeTransform } from '@angular/core';
import { GrocerieList } from './grocerie-lists.service';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'grocerieList'
})
export class GrocerieListPipe implements PipeTransform {
    constructor(public datePipe: DatePipe) { }

    transform(value: GrocerieList, ...args: unknown[]): string {
        if (value.endDate) {
            return 'vanaf : ' + this.datePipe.transform(value.startDate) + ' tot : ' + this.datePipe.transform(value.endDate);
        }

        return 'Huidige vanaf : ' + this.datePipe.transform(value.startDate);
    }
}
