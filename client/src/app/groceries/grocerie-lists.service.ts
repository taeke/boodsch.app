import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export interface GrocerieList {
    id: number;
    startDate: Date;
    endDate: Date;
}

@Injectable({
    providedIn: 'root'
})
export class GrocerieListsService {
    private grocerieListssUrl = 'api/grocerielists';

    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(private http: HttpClient) { }

    getGrocerieLists(): Observable<GrocerieList[]> {
        return this.http
            .get<GrocerieList[]>(this.grocerieListssUrl)
    }

    newGrocerieList(grocerieLists: GrocerieList[]): Observable<GrocerieList[]> {
        return this.http
            .post<GrocerieList>(this.grocerieListssUrl, {}, this.httpOptions)
            .pipe(map(grocerieList => {
                grocerieLists.unshift(grocerieList);
                return grocerieLists.slice(0);
            }));
    }
}
