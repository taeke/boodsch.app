import { TestBed } from '@angular/core/testing';
import { GrocerieListsService } from './grocerie-lists.service';
import { HttpClient } from '@angular/common/http';

class MockHttpClient {}

describe('GrocerieListsService', () => {
    let service: GrocerieListsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers:[{ provide: HttpClient, useClass: MockHttpClient }]
        });
        service = TestBed.inject(GrocerieListsService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
