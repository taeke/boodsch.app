import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Grocerie } from '../../groceries/groceries.service';
import { Store } from '../../stores/stores.service';
import { Department } from '../../departments/departments.service';
import { MatDialog } from '@angular/material/dialog';
import { RowEditComponent } from './row-edit/row-edit.component';
import { ObjectWithName } from '../../unique.name.validator';

@Component({
    selector: 'app-groceries-table',
    templateUrl: './groceries-table.component.html',
    styleUrls: ['./groceries-table.component.css']
})
export class GroceriesTableComponent {
    @Input() productNamesInGrocerieList: ObjectWithName[] = [];
    @Input() productNamesNotInGrocerieList: ObjectWithName[] = [];
    @Input() groceries: Grocerie[];
    @Input() canNotChange: boolean = true;
    @Input() stores: Store[] = [];
    @Input() departments: Department[] = [];
    @Output() deleteGrocerieEvent = new EventEmitter<Grocerie>();
    @Output() changeGrocerieEvent = new EventEmitter<Grocerie>();

    constructor(public dialog: MatDialog) { }

    displayedColumns: Array<string> = ['amount', 'product', 'store', 'department', 'delete'];

    editRow(row: Grocerie): void {
        const dialogRef = this.dialog.open(RowEditComponent, {
            width: '900px',
            data: {
                grocerie: row,
                stores: this.stores,
                departments: this.departments,
                productNamesInGrocerieList: this.productNamesInGrocerieList,
                productNamesNotInGrocerieList: this.productNamesNotInGrocerieList,
                groceries: this.groceries
            }
        });

        dialogRef.afterClosed().subscribe(updatedRow => {
            if (updatedRow !== undefined) {
                this.changeGrocerieEvent.emit(updatedRow);
            }
        });
    }

    delete(grocerie: Grocerie): void {
        this.deleteGrocerieEvent.emit(grocerie);
    }
}
