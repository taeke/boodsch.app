import { Component, Inject, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Grocerie } from '../../groceries.service';
import { Department } from '../../../departments/departments.service';
import { Store } from '../../../stores/stores.service';
import { ObjectWithName } from '../../../unique.name.validator';

export interface RowEdit {
    grocerie: Grocerie;
    stores: Store[];
    departments: Department[];
    productNamesInGrocerieList: ObjectWithName[];
    productNamesNotInGrocerieList: ObjectWithName[];
    groceries: Grocerie[];
}

@Component({
    selector: 'app-row-edit',
    templateUrl: './row-edit.component.html',
    styleUrls: ['./row-edit.component.css']
})
export class RowEditComponent {
    amount: number = 1;
    storeName: string = '';
    departmentName: string = '';
    productName: string = '';
    groceries: Grocerie[] = [];
    stores: Store[] = [];
    departments: Department[] = [];
    productNamesInGrocerieList: ObjectWithName[] = [];
    productNamesNotInGrocerieList: ObjectWithName[] = [];
    canNotChange = false;
    isNew: boolean = false;
    saveButtonName :string = 'Wijzigen';
    addGrocerieEvent = new EventEmitter();

    constructor(
        public dialogRef: MatDialogRef<RowEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: RowEdit) {
        this.amount = data.grocerie.amount;
        this.storeName = data.grocerie.storeName;
        this.departmentName = data.grocerie.departmentName;
        this.productName = data.grocerie.name;
        this.stores = data.stores;
        this.departments = data.departments;
        this.productNamesInGrocerieList = data.productNamesInGrocerieList;
        this.productNamesNotInGrocerieList = data.productNamesNotInGrocerieList;
        this.groceries = data.groceries;
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    addGrocerie(): void {
        this.dialogRef.close({
            id: this.data.grocerie.id,
            name: this.data.grocerie.name,
            grocerieList: this.data.grocerie.grocerieList,
            storeName: this.storeName,
            departmentName: this.departmentName,
            amount: this.amount
        });
    }
}
