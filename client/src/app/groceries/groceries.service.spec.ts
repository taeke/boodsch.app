import { TestBed } from '@angular/core/testing';
import { GroceriesService } from './groceries.service';
import { HttpClient } from '@angular/common/http';

class MockHttpClient {}

describe('GroceriesService', () => {
    let service: GroceriesService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers:[{ provide: HttpClient, useClass: MockHttpClient }]
        });
        service = TestBed.inject(GroceriesService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
