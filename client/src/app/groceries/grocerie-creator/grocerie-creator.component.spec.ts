import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { GrocerieCreatorComponent } from './grocerie-creator.component';
import { DepartmentsService } from '../../departments/departments.service';
import { ErrorStateMatcherService } from '../../error-state-matcher.service';
import { StoresService } from '../../stores/stores.service';
import { ProductsService } from '../../products/products.service';
import { GroceriesService } from '../groceries.service';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { Observable, of } from 'rxjs';

class MockErrorStateMatcherService {}

class MockDepartmentsService {
    getDepartments() {
        return of([]);
    }
}

class MockStoresService {
    getStores() {
        return of([]);
    }
}

class MockProductsService {
    getProducts() {
        return of([]);
    }
}

class MockGroceriesService {}

describe('GrocerieCreatorComponent', () => {
    let component: GrocerieCreatorComponent;
    let fixture: ComponentFixture<GrocerieCreatorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [GrocerieCreatorComponent],
            providers:[
                { provide: ErrorStateMatcherService, useClass: MockErrorStateMatcherService },
                { provide: DepartmentsService, useClass: MockDepartmentsService },
                { provide: StoresService, useClass: MockStoresService },
                { provide: ProductsService, useClass: MockProductsService },
                { provide: GroceriesService, useClass: MockGroceriesService },
            ],
            imports: [MatAutocompleteModule],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GrocerieCreatorComponent);
        component = fixture.componentInstance;
        component.groceriesInGrocerieList = [];
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
