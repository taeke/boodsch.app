import { Component, Output, EventEmitter, Input, OnInit, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Department } from '../../departments/departments.service';
import { Store } from '../../stores/stores.service';
import { FormControl, FormGroup, Validators, FormGroupDirective, AbstractControl } from '@angular/forms';
import { uniqueNameValidator } from '../../unique.name.validator';
import { ErrorStateMatcherService } from '../../error-state-matcher.service';
import { ObjectWithName } from '../../unique.name.validator';
import { Grocerie } from '../groceries.service';

@Component({
    selector: 'app-grocerie-creator',
    templateUrl: './grocerie-creator.component.html',
    styleUrls: ['./grocerie-creator.component.css']
})
export class GrocerieCreatorComponent implements OnChanges, OnInit {
    @Input() productNamesInGrocerieList: ObjectWithName[] = [];
    @Input() productNamesNotInGrocerieList: ObjectWithName[] = [];
    @Input() groceries: Grocerie[] = [];
    @Input() stores: Store[] = [];
    @Input() departments: Department[] = [];

    @Input() amount: number;
    @Output() amountChange = new EventEmitter<number>(); 

    @Input() productName: string;
    @Output() productNameChange = new EventEmitter<string>();

    @Input() storeName: string;
    @Output() storeNameChange = new EventEmitter<string>();
    
    @Input() departmentName: string;
    @Output() departmentNameChange = new EventEmitter<string>();

    @Output() addGrocerieEvent = new EventEmitter();

    @Input() canNotChange: boolean = true;
    @Input() isNew: boolean = true;
    @Input() saveButtonName :string = 'Toevoegen';
    
    @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

    productNamesFilteredByTypedName: Observable<ObjectWithName[]>;

    newGrocerieGroup = new FormGroup({
        amount: new FormControl(1, [Validators.required, Validators.min(1)]),
        productName: new FormControl('', [Validators.required]),
        storeName: new FormControl(null, [Validators.required]),
        departmentName: new FormControl(null, [Validators.required])
    });

    get amountControl(): AbstractControl { return this.newGrocerieGroup.get('amount'); }
    get productControl(): AbstractControl { return this.newGrocerieGroup.get('productName'); }
    get storeControl(): AbstractControl { return this.newGrocerieGroup.get('storeName'); }
    get departmentControl(): AbstractControl { return this.newGrocerieGroup.get('departmentName'); }

    constructor(public matcher: ErrorStateMatcherService) { }

    ngOnInit(): void {
        this.amountControl.valueChanges.subscribe(amount => { this.amountChange.emit(amount); })
        this.storeControl.valueChanges.subscribe(name => { this.storeNameChange.emit(name); });        
        this.departmentControl.valueChanges.subscribe(name => { this.departmentNameChange.emit(name); });
        this.productControl.valueChanges.subscribe(name => { this.productNameChange.emit(name); })
    }

    ngOnChanges(changes: SimpleChanges): void {
        if ('productNamesInGrocerieList' in changes && this.productNamesInGrocerieList) {
            this.productControl.setValidators([Validators.required, uniqueNameValidator(this.productNamesInGrocerieList)]);
        }
        
        if ('productNamesNotInGrocerieList' in changes && this.productNamesNotInGrocerieList) {
            this.productNamesFilteredByTypedName = this.productControl.valueChanges.pipe(startWith(''), map(value => this.filter(value)));
        }

        if ('amount' in changes) { this.amountControl.setValue(this.amount); }
        if ('storeName' in changes) { this.storeControl.setValue(this.storeName); }
        if ('departmentName' in changes) { this.departmentControl.setValue(this.departmentName); }
        if ('productName' in changes) { this.productControl.setValue(this.productName); }
    }

    onSubmit(): void {
        if ([this.productName, this.storeName, this.departmentName].hasNoEmptyStrings() && this.amount > 0 && (!this.groceries.hasName(this.productName) || !this.isNew)) {
            this.addGrocerieEvent.emit();
            setTimeout(() => { 
                this.formGroupDirective.resetForm();
                this.amountControl.setValue(1);
                this.productControl.setValue('');
                this.storeControl.setValue('');
                this.departmentControl.setValue('');
            }, 200);
        }
    }
    
    private filter(typedName: String): Array<ObjectWithName> {
        if (typedName == null) { return this.productNamesNotInGrocerieList }
        return this.productNamesNotInGrocerieList.filter(product => { return product.name.toLowerCase().startsWith(typedName.toLowerCase()) });
    }
}
