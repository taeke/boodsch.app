import { Component, OnInit } from '@angular/core';
import { GroceriesService, Grocerie, GrocerieImpl } from './groceries.service';
import { ObjectWithName } from '../unique.name.validator';
import { StoresService, Store } from '../stores/stores.service';
import { DepartmentsService, Department } from '../departments/departments.service';
import { GrocerieListsService, GrocerieList } from './grocerie-lists.service';
import { ProductsService, Product, ProductImpl } from '../products/products.service';
import { LoadingService } from '../loading.service';
import { DownLoadtype, DownloadService } from '../common-app/download.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-groceries',
    templateUrl: './groceries.component.html',
    styleUrls: ['./groceries.component.css']
})
export class GroceriesComponent implements OnInit {
    grocerieLists: GrocerieList[] = [];
    groceries: Grocerie[] = [];
    allProducts: Product[] = [];
    productNamesInGrocerieList: ObjectWithName[] = [];
    productNamesNotInGrocerieList: ObjectWithName[] = [];
    stores: Store[] = [];
    departments: Department[] = [];
    amount: number = 1;
    storeName: string;
    departmentName: string;
    canNotChange: boolean = true;
    isNew = true;
    saveButtonName :string = 'Toevoegen';
    _productName: string;
    get productName(): string { return this._productName; }
    set productName(name: string) { 
        this._productName = name; 
        this.setPreviousStoreAndDepartment();
    }

    _grocerieList: GrocerieList;
    get grocerieList(): GrocerieList { return this._grocerieList; }
    set grocerieList(grocerieList: GrocerieList) { 
        this._grocerieList = grocerieList; 
        this.canNotChange = Boolean(grocerieList.endDate);
        let call = [this.groceriesService.getGroceriesByGrocerieList(grocerieList)];
        let callOnFinish = () => { this.setProductLists(); };
        this.loadingService.load(this, call, ['groceries'], callOnFinish);
    }

    constructor(
        private loadingService: LoadingService,
        private storesService: StoresService, 
        private departmentsService: DepartmentsService,
        private productsService: ProductsService,
        private groceriesService: GroceriesService, 
        private downloadService: DownloadService,
        private grocerieListsService: GrocerieListsService,
        private datePipe: DatePipe) {}

    ngOnInit() : void {
        let calls = [
            this.storesService.getStores(), 
            this.departmentsService.getDepartments(), 
            this.grocerieListsService.getGrocerieLists(), 
            this.productsService.getProducts()];

        let callOnFinish = () => { if(this.grocerieLists.length > 0) { this.grocerieList = this.grocerieLists[0]; } };
        this.loadingService.load(this, calls, ['stores', 'departments', 'grocerieLists', 'allProducts'], callOnFinish);
    }

    addGrocerie(): void {
        let product = ProductImpl.create(this.productName, this.storeName, this.departmentName, this.amount);
        let grocerie = GrocerieImpl.create(this.grocerieList, this.amount, this.productName, this.storeName, this.departmentName);
        let calls = [
            this.productsService.addOrUpdateProduct(product, this.allProducts),
            this.groceriesService.addGrocerie(grocerie, this.groceries)];

        let callOnFinish = () => { this.setProductLists(); };
        this.loadingService.load(this, calls, ['allProducts', 'groceries'], callOnFinish);
    }

    downloadGrocerieList(downLoadtype: DownLoadtype): void {
        this.downloadService.donwload(this.groceries, `Boodschappenlijst - ${this.datePipe.transform(this.grocerieList.startDate)}.txt`, downLoadtype);
    }

    deleteGrocerie(grocerie: Grocerie): void {
        let call = [this.groceriesService.deleteGrocerie(grocerie, this.groceries)];
        this.loadingService.load(this, call, ['groceries'], () => {});
    }

    changeGrocerie(updatedGrocerie: Grocerie): void {
        let product = ProductImpl.create(updatedGrocerie.name, updatedGrocerie.storeName, updatedGrocerie.departmentName, updatedGrocerie.amount);
        let calls = [
            this.productsService.addOrUpdateProduct(product, this.allProducts),
            this.groceriesService.updateGrocerie(updatedGrocerie, this.groceries)];

        let callOnFinish = () => { this.setProductLists(); };
        this.loadingService.load(this, calls, ['allProducts', 'groceries'], callOnFinish);
    }

    newGrocerieList(): void {
        let call = [this.grocerieListsService.newGrocerieList(this.grocerieLists)];
        let callOnFinish = () => { 
            // Force reload otherwise mat-option grocerieList pipe will not re-render https://github.com/angular/components/issues/7938
            let innerCall = [this.grocerieListsService.getGrocerieLists()]
            let innerCallOnFinish = () => { this.grocerieList = this.grocerieLists[0]; };
            this.loadingService.load(this, innerCall, ['grocerieLists'], innerCallOnFinish); 
        };

        this.loadingService.load(this, call, ['grocerieLists'], callOnFinish);
    }

    private setPreviousStoreAndDepartment(): void {
        let product = this.allProducts.find(product => product.name === this.productName);
        if (product) {
            this.storeName = this.stores.hasName(product.lastStoreName) ? product.lastStoreName : '';
            this.departmentName = this.departments.hasName(product.lastDepartmentName) ? product.lastDepartmentName : '';
            this.amount = product.lastAmount > 0 ? product.lastAmount : 1;
        }
    }

    private setProductLists(): void {
        this.productNamesInGrocerieList = this.groceries.map(grocerie => { return { name: grocerie.name, id: grocerie.id }; });
        this.productNamesNotInGrocerieList = this.allProducts.difference(this.productNamesInGrocerieList);
    }
}
