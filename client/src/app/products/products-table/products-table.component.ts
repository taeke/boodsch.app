import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../products.service';

@Component({
    selector: 'app-products-table',
    templateUrl: './products-table.component.html',
    styleUrls: ['./products-table.component.css']
})
export class ProductsTableComponent {
    @Input() products: Product[];
    @Output() deleteProductEvent = new EventEmitter<Product>();
    @Output() updateProductEvent = new EventEmitter<Product>();

    displayedColumns: Array<string> = ['name', 'lastStoreName', 'lastDepartmentName', 'lastAmount', 'everyList', 'delete'];

    toggleEveryList(product: Product): void {
        product.everyList = !product.everyList;
        this.updateProductEvent.emit(product);
    }

    delete(product: Product): void {
        this.deleteProductEvent.emit(product);
    }
}
