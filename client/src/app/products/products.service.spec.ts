import { TestBed } from '@angular/core/testing';
import { ProductsService } from './products.service';
import { HttpClient } from '@angular/common/http';

class MockHttpClient {}

describe('ProductsService', () => {
    let service: ProductsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers:[{ provide: HttpClient, useClass: MockHttpClient }]
        });
        service = TestBed.inject(ProductsService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
