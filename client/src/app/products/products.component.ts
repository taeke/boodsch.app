import { Component, OnInit } from '@angular/core';
import { ProductsService, Product } from './products.service';
import { StoresService, Store } from '../stores/stores.service';
import { DepartmentsService, Department } from '../departments/departments.service';
import { LoadingService } from '../loading.service';
import { AlertDialogComponent } from '../common-app/alert-dialog/alert-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
    products: Product[] = [];
    stores: Store[] = [];
    departments: Department[] = [];

    constructor(
        private loadingService: LoadingService, 
        private productsService: ProductsService,
        private storesService: StoresService,
        private departmentsService: DepartmentsService,
        public dialog: MatDialog) { }

    ngOnInit(): void {
        let call = [this.productsService.getProducts(), this.departmentsService.getDepartments(), this.storesService.getStores()];
        this.loadingService.load(this, call, ['products', 'departments', 'stores'], () => {});
    }

    update(product: Product): void {
        this.checkUpdate(product);
        let call = [this.productsService.updateProduct(product, this.products)];
        this.loadingService.load(this, call, ['products'], () => {});
    }

    delete(product: Product): void {
        let call = [this.productsService.deleteProduct(product, this.products)];
        this.loadingService.load(this, call, ['products'], () => {});        
    }

    private checkUpdate(product: Product) {
        if(product.everyList && (this.stores.notHasName(product.lastStoreName) || this.departments.notHasName(product.lastDepartmentName))) {
            product.everyList = false;
            let message = `Dit product kan niet elke week toegevoegd worden want de winkel ${ product.lastStoreName } of
                de afdeling ${ product.lastDepartmentName } is niet bekend. Vermoedelijk omdat de winkel of de afdeling verwijderd is
                Voeg het product eenmalig handmatig toe aan een boodschappenlijst met een andere winkel of afdeling om dit op te lossen.`;
            this.dialog.open(AlertDialogComponent, { data: { message: message }});
        }
    }
}
