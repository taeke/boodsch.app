import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ObjectWithName } from '../unique.name.validator';
import { map } from 'rxjs/operators';
import { CommonToAppService } from '../common-app/common-to-app.service';

export interface Product extends ObjectWithName {
    lastStoreName: string;
    lastDepartmentName: string;
    lastAmount: number;
    everyList: boolean;
}

export class ProductImpl {
    static create(name: string, storeName: string, departmentName: string, amount: number): Product {
        return { name: name, lastStoreName: storeName, lastDepartmentName: departmentName, lastAmount: amount, everyList: false } as Product
    }
}

@Injectable({
    providedIn: 'root'
})
export class ProductsService {
    private productsUrl = 'api/products';

    private httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(private http: HttpClient, private commonToAppService: CommonToAppService) { }

    getProducts(): Observable<Product[]> {
        return this.http
            .get<Product[]>(this.productsUrl)
            .pipe(map(products => products.sort(this.commonToAppService.compare)));
    }

    addProduct(product: Product, products: Product[]): Observable<Product[]> {
        return this.http
            .post<Product>(this.productsUrl, product, this.httpOptions)
            .pipe(map(product => {
                products.push(product);
                return products.slice(0).sort(this.commonToAppService.compare);
            }));
    }

    addOrUpdateProduct(product: Product, products: Product[]): Observable<Product[]> {
        let existingProduct = products.find(p => p.name === product.name);
        if(existingProduct === undefined) {
            return this.addProduct(product, products);
        } else {
            return this.updateProduct({... product, id: existingProduct.id }, products);
        }
    }

    updateProduct(existingProduct: Product, products: Product[]): Observable<Product[]> {
        let newProduct = {... existingProduct, id: existingProduct.id, everyList: existingProduct.everyList };
        return this.http
            .put<Product>(this.productsUrl + "/" + newProduct.id, newProduct, this.httpOptions)
            .pipe(map(product => {
                products[products.indexOf(products.find(p => p.name === existingProduct.name))] = product;
                return products.slice(0).sort(this.commonToAppService.compare);
            }));
    }

    deleteProduct(product: Product, products: Product[]): Observable<Product[]> {
        return this.http
            .delete<Product>(this.productsUrl + "/" + product.id, this.httpOptions)
            .pipe(map(() =>{
                products.splice(products.indexOf(product), 1);
                return products.slice(0).sort(this.commonToAppService.compare);
            }));
    }
}
