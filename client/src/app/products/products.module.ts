import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products.component';
import { ProductsTableComponent } from './products-table/products-table.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CommonAppModule } from '../common-app/common-app.module';

@NgModule({
    declarations: [ProductsComponent, ProductsTableComponent],
    imports: [
        CommonModule, 
        MatCardModule, 
        MatFormFieldModule, 
        MatInputModule, 
        MatButtonModule, 
        MatTableModule, 
        MatPaginatorModule, 
        MatSortModule, 
        MatCheckboxModule, 
        ReactiveFormsModule,
        CommonAppModule
    ]
})
export class ProductsModule { }
