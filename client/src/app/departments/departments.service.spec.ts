import { TestBed } from '@angular/core/testing';
import { DepartmentsService } from './departments.service';
import { HttpClient } from '@angular/common/http';

class MockHttpClient {}

describe('DepartmentsService', () => {
    let service: DepartmentsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers:[{ provide: HttpClient, useClass: MockHttpClient }]
        });
        service = TestBed.inject(DepartmentsService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
