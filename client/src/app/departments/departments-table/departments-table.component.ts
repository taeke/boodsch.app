import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Department } from '../departments.service';

@Component({
    selector: 'app-departments-table',
    templateUrl: './departments-table.component.html',
    styleUrls: ['./departments-table.component.css']
})
export class DepartmentsTableComponent {
    @Input() departments: Department[];
    @Output() deleteDepartmentEvent = new EventEmitter<Department>();

    displayedColumns: Array<string> = ['name', 'delete'];

    delete(department: Department): void {
        this.deleteDepartmentEvent.emit(department);
    }
}
