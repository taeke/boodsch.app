import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DepartmentsComponent } from './departments.component';
import { DepartmentsService } from './departments.service';
import { Observable, of } from 'rxjs';

class MockDepartmentsService {
    getDepartments() {
        return of([]);
    }
}

describe('DepartmentsComponent', () => {
    let component: DepartmentsComponent;
    let fixture: ComponentFixture<DepartmentsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DepartmentsComponent],
            providers:[{ provide: DepartmentsService, useClass: MockDepartmentsService }],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DepartmentsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
