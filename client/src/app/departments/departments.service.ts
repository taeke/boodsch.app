import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ObjectWithName } from '../unique.name.validator';
import { map } from 'rxjs/operators';
import { CommonToAppService } from '../common-app/common-to-app.service';

export interface Department extends ObjectWithName {}

@Injectable({
    providedIn: 'root'
})
export class DepartmentsService {
    private departmentsUrl = 'api/departments';

    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    constructor(private http: HttpClient, private commonToAppService: CommonToAppService) { }

    getDepartments(): Observable<Department[]> {
        return this.http
            .get<Department[]>(this.departmentsUrl)
            .pipe(map(departments => departments.sort(this.commonToAppService.compare)));
    }

    addDepartment(department: Department, departments: Department[]): Observable<Department[]> {
        return this.http
            .post<Department>(this.departmentsUrl, department, this.httpOptions)
            .pipe(map(department => {
                departments.push(department);
                return departments.slice(0).sort(this.commonToAppService.compare);
            }));
    }

    deleteDepartment(department: Department, departments: Department[]): Observable<Department[]> {
        return this.http
            .delete<Department>(this.departmentsUrl + "/" + department.id, this.httpOptions)
            .pipe(map(() => {
                departments.splice(departments.indexOf(department), 1);
                return departments.slice(0).sort(this.commonToAppService.compare);
            }));
    }
}
