import { Component, ViewChild, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators, FormGroupDirective, AbstractControl } from '@angular/forms';
import { Department } from 'src/app/departments/departments.service';
import { uniqueNameValidator } from '../../unique.name.validator';
import { ErrorStateMatcherService } from '../../error-state-matcher.service';

@Component({
    selector: 'app-department-creator',
    templateUrl: './department-creator.component.html',
    styleUrls: ['./department-creator.component.css']
})
export class DepartmentCreatorComponent implements OnChanges {
    @Output() newDepartmentEvent = new EventEmitter<string>();
    @Input() departments: Department[];
    @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

    newDepartmentGroup = new FormGroup({
        name: new FormControl('', [Validators.required])
    });

    get nameControl(): AbstractControl { return this.newDepartmentGroup.get('name'); }

    constructor(public matcher: ErrorStateMatcherService) { }

    ngOnChanges(changes: SimpleChanges): void {
        if ('departments' in changes && typeof this.departments !== 'undefined' && this.departments.length > 0) {
            this.nameControl.setValidators([Validators.required, uniqueNameValidator(this.departments)]);
        }
    }

    onSubmit(): void {
        let name = this.newDepartmentGroup.value.name;
        if (name && name != '' && !this.departments.find(item => item.name === name)) {
            this.newDepartmentEvent.emit(name);
            setTimeout(() => this.formGroupDirective.resetForm(), 200);
        }
    }
}
