import { CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { DepartmentCreatorComponent } from './department-creator.component';

describe('DepartmentCreatorComponent', () => {
    let component: DepartmentCreatorComponent;
    let fixture: ComponentFixture<DepartmentCreatorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [],
            declarations: [DepartmentCreatorComponent],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DepartmentCreatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
