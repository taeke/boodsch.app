import { Component, OnInit } from '@angular/core';
import { Department, DepartmentsService } from './departments.service';
import { LoadingService } from '../loading.service';
import { ProductsService, Product } from '../products/products.service';
import { AlertDialogComponent } from '../common-app/alert-dialog/alert-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
    selector: 'app-departments',
    templateUrl: './departments.component.html',
    styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
    departments: Department[] = [];
    products: Product[] = [];

    constructor(
        private loadingService: LoadingService, 
        private departmentsService: DepartmentsService,
        private productsService: ProductsService,
        public dialog: MatDialog) { }

    ngOnInit(): void {
        let calls = [this.departmentsService.getDepartments(), this.productsService.getProducts()];
        this.loadingService.load(this, calls, ['departments', 'products'], () => {});
    }

    add(name: string): void {
        name = name.trim();
        if (name) { 
            let call = [this.departmentsService.addDepartment({ name } as Department, this.departments)];
            this.loadingService.load(this, call, ['departments'], () => {});
        }
    }

    delete(department: Department): void {
        if(this.canDelete(department, this.products.filter(product => product.everyList && product.lastDepartmentName === department.name))) {
            let call = [this.departmentsService.deleteDepartment(department, this.departments)];
            this.loadingService.load(this, call, ['departments'], () => {});
        }
    }

    private canDelete(department: Department, productsWithDepartment: Product[]): boolean {
        if(productsWithDepartment.length > 0) {
            let message = `De afdeling ${ department.name } kan niet verwijderd worden want de volgende producten worden elke keer
                toegevoegd en gebruiken deze afdeling: ${ productsWithDepartment.map(product => product.name).join(",") }. 
                Verwijder het vinkje elke keer toevoegen en voeg ze eenmalig handmatig toe aan een boodschappenlijst met een andere 
                afdeling om dit op te lossen.`;
            this.dialog.open(AlertDialogComponent, { data: { message: message }});
            return false;  
        } 

        return true;
    }
}
