import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentsComponent } from './departments.component';
import { DepartmentCreatorComponent } from './department-creator/department-creator.component';
import { DepartmentsTableComponent } from './departments-table/departments-table.component';
import { ReactiveFormsModule } from '@angular/forms';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { CommonAppModule } from '../common-app/common-app.module';

@NgModule({
    declarations: [DepartmentsComponent, DepartmentCreatorComponent, DepartmentsTableComponent],
    imports: [
        CommonModule, 
        MatCardModule, 
        MatFormFieldModule, 
        MatInputModule, 
        MatButtonModule, 
        MatTableModule, 
        MatPaginatorModule, 
        MatSortModule, 
        ReactiveFormsModule,
        CommonAppModule
    ]
})
export class DepartmentsModule { }
