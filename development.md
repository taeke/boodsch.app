# Development
The only things you need on your development machine is docker and a decent editor: I am using https://code.visualstudio.com/. Make sure to use the Hyper V option if you use docker on windows otherwise the hot reloading does not seem to work.

To start both client and server local:
```
docker-compose up -d start
```

To create a production docker image on your local machine (use correct current SNAPSHOT version of course):
```
docker build --file local.Dockerfile --tag boodsch.app.local --build-arg JARVERSION=boodsch.app.server-0.1.3-SNAPSHOT .
```

To locally run a production docker image:
```
docker-compose -f .\pi\docker-compose.local.yaml up -d server
```
The client will be avialable on http://localhost:4300/

## server
To start a local version run:
```
docker-compose up -d server
```

Or if you need to see the logging
```
docker-compose up server
```

Start both back-end and front-and
```
docker-compose up -d start
```

To run the tests:
```
docker-compose run --rm mvnw -f server/pom.xml clean test
```

To generate a diff file for liquibase. 
```
docker-compose run --rm mvnw -f server/pom.xml clean compile
docker-compose run --rm mvnw -f server/pom.xml liquibase:diff
```
Resulting changeset name will be including the SNAPSHOT tag. Remove this part of the name and add the changed name to changelog-master.xml.

To run the maintain container
```
docker-compose run --rm maintain
```

To connect to the postgress container from the maintain container
```
psql -h db -U postgres
```

## client
To start just the client:
```
docker-compose up client
```

To run the tests:
```
docker-compose run yarn test
```

To add an extra package:
```
docker-compose run --rm yarn add @material-ui/core
```

## release
To create a release (Create a git tag with a release version and commit the new snapshot version):
```
docker-compose run --rm mvnw release:prepare
docker-compose run --rm mvnw release:clean
git push origin boodsch.app.server-0.1.0
```
The clean is needed to remove some release files which could be removed by release:perform, but we are not really releasing in that sense because we create a jar inside a docker container instead of releasing it into a maven repository. The release plugin is just used to update the version and create a git tag.

# Useful docker commands
## Containers (list, remove and list again)
```
docker ps -a

docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)

docker ps -a
```

## Images (list, remove and list again)
```
docker images -a
docker rmi $(docker images -a -q)
docker images -a
```

## Volumes  (list, remove and list again)
```
docker volume ls
docker volume prune
docker volume rm boodschapp_db-data
docker volume ls
```

# curl
From the maintain container
```
curl -X POST server:8080/products -H 'Content-type:application/json' -d '{"name": "Kaas"}'
curl -v server:8080/products
curl -v server:8080/products/1
curl -X PUT server:8080/products/1 -H 'Content-type:application/json' -d '{"name": "Kaasje"}'
curl -X DELETE server:8080/products/1
```
