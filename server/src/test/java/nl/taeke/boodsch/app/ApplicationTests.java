package nl.taeke.boodsch.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;

@SpringBootTest
@EnableAutoConfiguration(exclude=LiquibaseAutoConfiguration.class)
class ProductApplicationTests {

	@Test
	void contextLoads() {
	}

}
