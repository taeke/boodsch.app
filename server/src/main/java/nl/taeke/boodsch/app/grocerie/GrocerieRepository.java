package nl.taeke.boodsch.app.grocerie;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GrocerieRepository extends JpaRepository<Grocerie, Long> {
}