package nl.taeke.boodsch.app.grocerie;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.*;
import nl.taeke.boodsch.app.grocerielist.GrocerieList;

@Data
@Table(name = "grocerie")
@Entity
public class Grocerie {

    private @Id @GeneratedValue Long id;
    private String name;
    private int amount;

    @Column(name = "store_name")
    private String storeName;

    @Column(name = "department_name")
    private String departmentName;

    @ManyToOne
    private GrocerieList grocerieList;

    Grocerie() {
    }

    public Grocerie(String name, String storeName, String departmentName, int amount, GrocerieList grocerieList) {
        this.name = name;
        this.storeName = storeName;
        this.departmentName = departmentName;
        this.amount = amount;
        this.grocerieList = grocerieList;
    }
}
