package nl.taeke.boodsch.app.product;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.*;

@Data
@Table(name = "product")
@Entity
public class Product {

    private @Id @GeneratedValue Long id;
    private String name;

    @Column(name = "last_store_name")
    private String lastStoreName;

    @Column(name = "last_department_name")
    private String lastDepartmentName;

    @Column(name = "lastAmount")
    private int lastAmount;

    @Column(name = "every_list")
    private boolean everyList;

    Product() {
    }

    Product(String name, String lastStoreName, String lastDepartmentName, int lastAmount, boolean everyList) {
        this.name = name;
        this.lastStoreName = lastStoreName;
        this.lastDepartmentName = lastDepartmentName;
        this.lastAmount = lastAmount;
        this.everyList = everyList;
    }
}
