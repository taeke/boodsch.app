package nl.taeke.boodsch.app.product;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductController {

    private final ProductRepository repository;

    ProductController(ProductRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/products")
    List<Product> all() {
        return repository.findAll();
    }

    @GetMapping("/api/products/{id}")
    Product one(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
    }

    @PostMapping("/api/products")
    Product newProduct(@RequestBody Product newProduct) {
        return repository.save(newProduct);
    }

    @PutMapping("/api/products/{id}")
    Product replaceProduct(@RequestBody Product newProduct, @PathVariable Long id) {

        return repository.findById(id).map(product -> {
            product.setEveryList(newProduct.isEveryList());
            product.setLastStoreName(newProduct.getLastStoreName());
            product.setLastDepartmentName(newProduct.getLastDepartmentName());
            product.setLastAmount(newProduct.getLastAmount());
            return repository.save(product);
        }).orElseGet(() -> {
            newProduct.setId(id);
            return repository.save(newProduct);
        });
    }

    @DeleteMapping("/api/products/{id}")
    void deleteProduct(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
