package nl.taeke.boodsch.app.store;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.*;

@Data
@Table(name = "store")
@Entity
class Store {

    private @Id @GeneratedValue Long id;
    private String name;

    Store() {
    }

    Store(String name) {
        this.name = name;
    }
}
