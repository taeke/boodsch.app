package nl.taeke.boodsch.app.grocerie;

public class GrocerieNotFoundException extends RuntimeException {
    GrocerieNotFoundException(Long id) {
        super("Could not find grocerie " + id);
    }
}