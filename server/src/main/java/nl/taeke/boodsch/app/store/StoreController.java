package nl.taeke.boodsch.app.store;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StoreController {

    private final StoreRepository repository;

    StoreController(StoreRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/stores")
    List<Store> all() {
        return repository.findAll();
    }

    @GetMapping("/api/stores/{id}")
    Store one(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new StoreNotFoundException(id));
    }

    @PostMapping("/api/stores")
    Store newStore(@RequestBody Store newStore) {
        return repository.save(newStore);
    }

    @PutMapping("/api/stores/{id}")
    Store replaceStore(@RequestBody Store newStore, @PathVariable Long id) {

        return repository.findById(id).map(store -> {
            store.setName(newStore.getName());
            return repository.save(store);
        }).orElseGet(() -> {
            newStore.setId(id);
            return repository.save(newStore);
        });
    }

    @DeleteMapping("/api/stores/{id}")
    void deleteStore(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
