package nl.taeke.boodsch.app.grocerielist;

public class GrocerieListNotFoundException extends RuntimeException {
    GrocerieListNotFoundException(Long id) {
        super("Could not find grocerieList " + id);
    }
}