package nl.taeke.boodsch.app.grocerielist;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.*;
import java.time.LocalDate;

@Data
@Table(name = "grocerielist")
@Entity
public class GrocerieList {

    private @Id @GeneratedValue Long id;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    GrocerieList() {
    }

    GrocerieList(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }
}
