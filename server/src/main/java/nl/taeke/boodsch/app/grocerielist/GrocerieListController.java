package nl.taeke.boodsch.app.grocerielist;

import java.time.LocalDate;
import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import nl.taeke.boodsch.app.product.ProductRepository;
import nl.taeke.boodsch.app.grocerie.GrocerieRepository;
import nl.taeke.boodsch.app.grocerie.Grocerie;

@RestController
public class GrocerieListController {

    private final GrocerieListRepository repository;
    private final ProductRepository productRepository;
    private final GrocerieRepository grocerieRepository;

    GrocerieListController(GrocerieListRepository repository, ProductRepository productRepository, GrocerieRepository grocerieRepository) {
        this.repository = repository;
        this.productRepository = productRepository;
        this.grocerieRepository = grocerieRepository;
    }

    @GetMapping("/api/grocerielists")
    List<GrocerieList> all() {
        return repository.findAll();
    }

    @GetMapping("/api/grocerielists/{id}")
    GrocerieList one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new GrocerieListNotFoundException(id));
    }

    @PostMapping("/api/grocerielists")
    GrocerieList newGrocerieList(@RequestBody GrocerieList newGrocerieList) {
        repository.findAll().forEach(grocerieList -> {
                if(grocerieList.getEndDate() == null) {
                    grocerieList.setEndDate(LocalDate.now());
                    repository.save(grocerieList);
                }
            }
        );

        GrocerieList grocerieList = repository.save(new GrocerieList(LocalDate.now(), null));

        productRepository.findAll().forEach(product -> {
            if(product.isEveryList()) {
                Grocerie grocerie = new Grocerie(product.getName(), product.getLastStoreName(), product.getLastDepartmentName(), product.getLastAmount(), grocerieList);
                this.grocerieRepository.save(grocerie);
            }
        });

        return grocerieList;
    }

    @PutMapping("/api/grocerielists/{id}")
    GrocerieList replaceGrocerieList(@RequestBody GrocerieList newGrocerieList, @PathVariable Long id) {

        return repository.findById(id).map(grocerielist -> {
            return repository.save(grocerielist);
        }).orElseGet(() -> {
            newGrocerieList.setId(id);
            return repository.save(newGrocerieList);
        });
    }

    @DeleteMapping("/api/grocerielists/{id}")
    void deleteGrocerieList(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
