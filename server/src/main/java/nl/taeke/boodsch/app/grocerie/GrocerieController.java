package nl.taeke.boodsch.app.grocerie;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GrocerieController {

    private final GrocerieRepository repository;

    GrocerieController(GrocerieRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/groceries")
    List<Grocerie> all() {
        return repository.findAll();
    }

    @GetMapping("/api/groceries/{id}")
    Grocerie one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new GrocerieNotFoundException(id));
    }

    @PostMapping("/api/groceries")
    Grocerie newGrocerie(@RequestBody Grocerie newGrocerie) {
        return repository.save(newGrocerie);
    }

    @PutMapping("/api/groceries/{id}")
    Grocerie replaceGrocerie(@RequestBody Grocerie newGrocerie, @PathVariable Long id) {

        return repository.findById(id).map(grocerie -> {
            grocerie.setName(newGrocerie.getName());
            grocerie.setStoreName(newGrocerie.getStoreName());
            grocerie.setDepartmentName(newGrocerie.getDepartmentName());
            grocerie.setAmount(newGrocerie.getAmount());
            return repository.save(grocerie);
        }).orElseGet(() -> {
            newGrocerie.setId(id);
            return repository.save(newGrocerie);
        });
    }

    @DeleteMapping("/api/groceries/{id}")
    void deleteGrocerie(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
