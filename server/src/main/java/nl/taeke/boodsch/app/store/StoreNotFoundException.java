package nl.taeke.boodsch.app.store;

public class StoreNotFoundException extends RuntimeException {
    StoreNotFoundException(Long id) {
        super("Could not find store " + id);
    }
}