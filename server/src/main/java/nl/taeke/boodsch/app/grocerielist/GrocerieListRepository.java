package nl.taeke.boodsch.app.grocerielist;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
interface GrocerieListRepository extends JpaRepository<GrocerieList, Long> {
}