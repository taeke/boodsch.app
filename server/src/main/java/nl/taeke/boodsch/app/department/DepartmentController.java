package nl.taeke.boodsch.app.department;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DepartmentController {

    private final DepartmentRepository repository;

    DepartmentController(DepartmentRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/api/departments")
    List<Department> all() {
        return repository.findAll();
    }

    @GetMapping("/api/departments/{id}")
    Department one(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new DepartmentNotFoundException(id));
    }

    @PostMapping("/api/departments")
    Department newDepartment(@RequestBody Department newDepartment) {
        return repository.save(newDepartment);
    }

    @PutMapping("/api/departments/{id}")
    Department replaceDepartment(@RequestBody Department newDepartment, @PathVariable Long id) {

        return repository.findById(id).map(department -> {
            department.setName(newDepartment.getName());
            return repository.save(department);
        }).orElseGet(() -> {
            newDepartment.setId(id);
            return repository.save(newDepartment);
        });
    }

    @DeleteMapping("/api/departments/{id}")
    void deleteDepartment(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
