# Build the front-end
FROM node:14.1.0-alpine3.11 AS ANGULAR_BUILD
RUN mkdir app
WORKDIR /app
COPY ./client/. ./
RUN npm install -g @angular/cli
RUN npm install --unsafe-perm
RUN ng build

# Build the back-end with the front-end as a resource
FROM openjdk:8-jdk-alpine AS MAVEN_BUILD
COPY ./server/. /build/
COPY ./mvnw /build/
COPY ./.mvn /build/.mvn/
COPY --from=ANGULAR_BUILD /app/dist/boodschapp /build/src/main/resources/static
WORKDIR /build/
RUN chmod +x ./mvnw
RUN ./mvnw clean package

# Create a image with the back-end jar started
FROM openjdk:8-jdk-alpine
ARG JARVERSION
RUN mkdir app
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/$JARVERSION.jar /app/
RUN echo "java -jar /app/$JARVERSION.jar" > ./entrypoint.sh
RUN chmod +x ./entrypoint.sh
ENTRYPOINT ./entrypoint.sh
