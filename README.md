# BoodschApp
Every saterday we created a handwritten list for buying groceries which contain a lot of the same items every week. We needed to remember to put the same items on the list every week and write out all the products even if we have bought them before. Also this list was rewritten in the order we can find the products in the stores. So we even wrote the list twice. This is an App to automate this process. The name is a play on the dutch word for groceries: Boodschappen and the word App.

# Usage
The app is build in https://angular.io/ and is served from a spring boot application witch also contains the REST API it needs. It is started in a docker container. It stores it's data in a PostgreSQL database which is run in it's own docker container. I run both docker containers on a Raspberry Pi 4 Model B 4G together with https://github.com/containrrr/watchtower so it automatically updates. See the folder pi how to run this.

# Todo
- Update development.md with changes for angular;
- Check every thing and create needed actions to clean up if any;
- Fix Your global Angular CLI version (10.2.0) is greater than your local version (10.0.6). The local Angular CLI version is used;

# Future features
- Show more groceries on one web page;
- Add a button in the products table to directly put a product in the current grocerielist;
- Add a boolean special offer;
- Add dishes: A collection of products needed to create a dish and an option to add them to the currect grocerielist;
- Add a wishlist. Products which can be placed on the grocerielist when the time is come to create the list and the time is come to decide which store to buy it.



