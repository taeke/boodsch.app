FROM openjdk:8-jdk-alpine

# Needed for maven-release-plugin
RUN apk update && apk add git && apk add npm
ARG GIT_EMAIL
ARG GIT_NAME
RUN git config --global user.email "${GIT_EMAIL}"
RUN git config --global user.name "${GIT_NAME}"

# /app   is where development files are bound. 
# /maven is where the script making mvnw script executable is stored 
#        it also calls the mvnw script.
RUN mkdir /app
RUN mkdir /maven

WORKDIR /maven
COPY ./mvnw-entrypoint.sh .
RUN chmod +x ./mvnw-entrypoint.sh
ENTRYPOINT ["./mvnw-entrypoint.sh"]
